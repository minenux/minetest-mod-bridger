# VenenuX minetest mod `bridger`

**bridges complex** advanced nodes conducive to building complex bridges

## Information

this mod is named `bridger` and adds large number of advanced nodes conducive 
to building large, industrial bridges.

![](screenshot.png?raw=true)

Drawbridges/swivel bridges can be opened and shut with mesecons.
The off state is shut, and the on state is open.

## Technical info

No crafting guide in game yet, the code is not so clean ***but if it ain't broke, don't fix it***

#### Dependences

The list of dependencies are in order or relevance and priority to work:

Required to work:

* default
* stairs

Optional Recommended

* moreblocks
* stairsplus

## Crafting

The file [crafts.lua](crafts.lua) have all the registered nodes, there are larger amount of.

Original forum post https://forum.minetest.net/viewtopic.php?t=18243

## LICENCE

* Code: MIT
* Media: CC BY-SA 3.0

check [LICENCE](LICENCE)

